package com.t1.yd.tm.component;

import com.t1.yd.tm.api.controller.ICommandController;
import com.t1.yd.tm.api.controller.IProjectController;
import com.t1.yd.tm.api.controller.ITaskController;
import com.t1.yd.tm.api.repository.ICommandRepository;
import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.service.ICommandService;
import com.t1.yd.tm.api.service.IProjectService;
import com.t1.yd.tm.api.service.ITaskService;
import com.t1.yd.tm.constant.ArgumentConstant;
import com.t1.yd.tm.constant.CommandConstant;
import com.t1.yd.tm.controller.CommandController;
import com.t1.yd.tm.controller.ProjectController;
import com.t1.yd.tm.controller.TaskController;
import com.t1.yd.tm.repository.CommandRepository;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;
import com.t1.yd.tm.service.CommandService;
import com.t1.yd.tm.service.ProjectService;
import com.t1.yd.tm.service.TaskService;
import com.t1.yd.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ITaskController taskController = new TaskController(taskService);

    public void run(String[] args) {
        commandController.showWelcome();
        processArguments(args);

        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            processCommand(TerminalUtil.nextLine());
        }
    }

    private void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
    }

    private void processArgument(String arg) {
        switch (arg) {
            case ArgumentConstant.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.INFO:
                commandController.showInfo();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showArgumentError();
        }
    }

    private void processCommand(String arg) {
        switch (arg) {
            case CommandConstant.ABOUT:
                commandController.showAbout();
                break;
            case CommandConstant.VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.HELP:
                commandController.showHelp();
                break;
            case CommandConstant.EXIT:
                exit();
                break;
            case CommandConstant.INFO:
                commandController.showInfo();
                break;
            case CommandConstant.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConstant.PROJECT_GET_BY_ID:
                projectController.showById();
                break;
            case CommandConstant.PROJECT_GET_BY_INDEX:
                projectController.showByIndex();
                break;
            case CommandConstant.PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case CommandConstant.PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case CommandConstant.PROJECT_REMOVE_BY_ID:
                projectController.removeById();
                break;
            case CommandConstant.PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case CommandConstant.PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case CommandConstant.PROJECT_COMPLETE_BY_ID:
                projectController.completeById();
                break;
            case CommandConstant.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeByIndex();
                break;
            case CommandConstant.PROJECT_UPDATE_STATUS_BY_ID:
                projectController.changeStatusById();
                break;
            case CommandConstant.PROJECT_UPDATE_STATUS_BY_INDEX:
                projectController.changeStatusByIndex();
                break;
            case CommandConstant.PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case CommandConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConstant.TASK_GET_BY_ID:
                taskController.showById();
                break;
            case CommandConstant.TASK_GET_BY_INDEX:
                taskController.showByIndex();
                break;
            case CommandConstant.TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case CommandConstant.TASK_START_BY_ID:
                taskController.startById();
                break;
            case CommandConstant.TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case CommandConstant.TASK_COMPLETE_BY_ID:
                taskController.completeById();
                break;
            case CommandConstant.TASK_COMPLETE_BY_INDEX:
                taskController.completeByIndex();
                break;
            case CommandConstant.TASK_UPDATE_STATUS_BY_ID:
                taskController.changeStatusById();
                break;
            case CommandConstant.TASK_UPDATE_STATUS_BY_INDEX:
                taskController.changeStatusByIndex();
                break;
            case CommandConstant.TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case CommandConstant.TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case CommandConstant.TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            default:
                commandController.showCommandError();
        }
    }

    private void exit() {
        System.exit(0);
    }

}
