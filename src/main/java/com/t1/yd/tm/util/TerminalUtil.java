package com.t1.yd.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String s = SCANNER.nextLine();
        return Integer.parseInt(s);
    }

}
